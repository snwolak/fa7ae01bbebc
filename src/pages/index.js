import React from 'react'
import Link from 'gatsby-link'
import styled from 'styled-components'

import Menu from '../components/Menu'
import ArticleCard from '../components/ArticleCard'
import Offer from '../components/Offer'
const Container = styled.div`
padding-top: 42px;
height: 852px;
display: flex;
align-items: center;
box-sizing:border-box;
background-color: #122636;

flex-direction: column;

`
const HeroTitle = styled.div`
  margin-top: 163px;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  box-sizing: border-box;
  padding-top: 10px;
  width: 505px;
  font-family: 'Raleway', sans-serif;
  font-size: 72px;
  color: #fff;
  text-transform: uppercase;
  font-weight: 800;
  line-height: 60px;
  hr {
    margin: 0;
    margin-bottom: 10px;
    margin-left: 4px;
    border: 0;
    background-color: #fdc300;
    width: 66px;
    height: 3px;
  }
`
const ArticleContainer = styled.div`
  display: flex;
  justify-content: center;
  flex-direction: row;

  background-color: #1b2836;
  height: 336px;
`
const OfferSection = styled.section`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  height: 648px;
  
`
const OfferSectionTitle = styled.div`
text-align: center;
font-family: 'Raleway', sans-serif;
 h3 {
  font-size: 30px;
  line-height: 20px;
  color: rgb(51,51,51);
  
  font-weight: 600;
 }
 p {
  font-size: 13px;
  line-height: 20px;
  color: rgb(136,136,136);
  
  text-transform: uppercase;
 }
 hr {
  border: 0;
  width: 30px;
  height: 3px;
  background-color: #fdc300;
 }
`
const OfferContainer = styled.div`
  width: 1144px;
  display: grid;
  grid-template-columns: 50% 50%;
`
const IndexPage = ({ data }) => (
  <div>
  <Container>
    <div>
    <Menu />
    <HeroTitle><hr/>strongest distribution network</HeroTitle>
    </div>
    
  </Container>
  
  <ArticleContainer>
      {data.allContentfulArticle.edges.map(item => {
        return <ArticleCard key={item.node.id} data={item.node} />
      })}
  </ArticleContainer >
  <OfferSection>
    <OfferSectionTitle >
      <h3>What we Offer</h3>
      <p>tailored logistic services</p>
      <hr />
      </OfferSectionTitle>
  <OfferContainer>
      {data.allContentfulOffer.edges.map(item => {
        return <Offer key={item.node.id} data={item.node}/>
      })}
  </OfferContainer>
  </OfferSection>
  </div>
)

export default IndexPage
export const query = graphql`
query ArticleQuery {
  allContentfulArticle {
    edges {
      node {
        id,
        title,
        description
      }
    }
  }
  allContentfulOffer {
    edges {
      node {
        id
        title,
        description,
        icon {
          id,
          file {
            url
            fileName
          }
        },
      }
    }
  }
}

  

`