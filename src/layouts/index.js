import React from 'react'
import PropTypes from 'prop-types'
import Helmet from 'react-helmet'
import styled, {injectGlobal} from 'styled-components'
injectGlobal`
@import url('https://fonts.googleapis.com/css?family=Raleway:400,500,600,900');
  @font-face {
    font-family: 'Raleway', sans-serif;
  }

  body {
    margin: 0;
  }
`;
const Container = styled.div`

  margin: 0;
`
const Layout = ({ children, data }) => (
  
  <Container>
     
      {children()}

    
  </Container>
)

Layout.propTypes = {
  children: PropTypes.func,
}

export default Layout


