import React, { Component } from 'react'
import Link from 'gatsby-link'
import styled from 'styled-components'
import Icon from 'react-icons-kit';
import {ic_search} from 'react-icons-kit/md/ic_search'
import logo from '../icons/logo.svg'
const Container = styled.div`

box-sizing:border-box;
display: grid;
align-items: center;

grid-template-columns: 30% auto;
width: calc(100vw * 0.59375);
padding: 0px 30px 0px 30px;
height: 81px;
background-image: linear-gradient(0deg, #fbfbfb 0%, #ffffff 100%);
border-radius: 3px;
box-shadow: 0px -8px 0px rgba(255,255,255,0.2);
nav {
  top: 0;
  display: flex;
  justify-content: flex-end;
  align-items: center;
  box-sizing:border-box;
  height: 81px;
}
a {
  box-sizing:border-box;
  text-decoration: none;
  display: flex;
  align-items: center;
  margin-right: 10px;
  height: 81px;
 
}
a:hover {
  border-bottom: 3px solid #006DB7;
}

`
const LogoContainer = styled.div`
  display: flex;
  flex-direction: row;
  font-family: 'Raleway', sans-serif;
  img {
    margin-right: 10px;
  }
  div {
    display: flex;
    flex-direction: column;
  }
  span {
    font-size: 26px;
    font-weight: 900;
    text-transform: uppercase;

  }
  small {
    color: #777777;
    font-size: 11px;
    margin-left: -5px;
  }
`
const MenuBtn = styled.button`

  
  box-sizing:border-box;
  padding: 0px 10px 0px 10px;
  cursor: pointer;
  border: 0;
  outline: 0;
  background-color: #fff;
  text-transform: uppercase;
  text-decoration: none;
  font-size: 13px;
  font-weight: 700;
  line-height: 22px;
  color: rgb(51,51,51);

`
const IconContainer = styled.header`
box-sizing: border-box;
display: flex;
align-items: center;
color: #fff;
border-radius: 50%;
padding: 5px;
background-color: #006DB7;

`
const Logo = styled.img`
  width: 28px;
  height: 39px;
`
export default class Menu extends Component {
  renderMenu = () => {
    const links = ['Home', 'About us', 'Services', 'News', 'Locations', 'Contact']
    return links.map(link => {
      return <Link key={link} to={'/' + link.replace(" ", '').toLowerCase() + '/'}><MenuBtn>{link.toUpperCase()}</MenuBtn></Link>
    })
  }
  render() {
    return (
      <Container>
        <LogoContainer>
          <Logo src={logo}/>
          <div>
            <span>trucking</span>
            <small>Logistics & Transportation</small>
          </div>
        </LogoContainer>
        <nav>
        {this.renderMenu()}
        <IconContainer><Icon icon={ic_search}/></IconContainer>
        </nav>
      </Container>
    )
  }
}
