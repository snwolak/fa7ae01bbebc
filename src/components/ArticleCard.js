import React, { Component } from 'react'
import Link from 'gatsby-link'
import styled from 'styled-components'
import Icon from 'react-icons-kit';
import {ic_keyboard_arrow_right} from 'react-icons-kit/md/ic_keyboard_arrow_right'


const Container = styled.div`
box-sizing:border-box;
background-color: #fff;
height: 400px;
width: 361px;
margin-right: 18px;
margin-top: -200px;
}
`
const Featured = styled.div`
background-color: #e4e4e4;
height: 200px;
position: relative;
`
const ReadBtn = styled.button`
cursor: pointer;
position: absolute;
bottom: 0;
padding: 0;
right: 0;
border: 0;
outline:0;
width: 165px;
height: 34px;

background-color: #1b2935;
display: flex;
flex-direction: row;
justify-content: flex-end;
color: #fff;

&:hover {
  background-color: #006db7;
  span {
    background-color: #037dd1;
  }
}

`
const BtnText = styled.div`
  height: 34px;
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: center;

  font-size: 13px;
  font-weight: 700;
  text-transform: uppercase;
`
const IconContainer = styled.span`
width: 30px;
height: 34px;
background-color: #2f373f;
display: flex;
align-items: center;
justify-content: center;

`
const Text = styled.div`
  padding: 30px 30px 0 30px;
  h3 {
    margin: 0;
    text-transform: uppercase;
    font-weight: 800;
    
    color: rgb(51,51,51);
  }
  hr {
    margin: 0;
    margin-bottom: 10px;
    border: 0;
    width: 30px;
    height: 3px;
    background-color: #fdc300;
  }
  p {
    color: rgb(119,119,119);
    font-family: "Open Sans";
    line-height: 20px;
  }
`
export default class Menu extends Component {
  
  render() {
    return (
      <Container>
        <Featured>
          <ReadBtn>
            <BtnText>
              read more 
            </BtnText>
            <IconContainer><Icon icon={ic_keyboard_arrow_right} /></IconContainer></ReadBtn>
        </Featured>
        <Text>
          <hr/>
          <h3>{this.props.data.title}</h3>
          <p>{this.props.data.description}</p>
        </Text>
        
      </Container>
    )
  }
}
