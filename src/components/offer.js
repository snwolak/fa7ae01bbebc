import React, { Component } from 'react'
import styled from 'styled-components'
const Container = styled.div`
box-sizing: border-box;
display: flex;
flex-direction: row;
width: 555px;
height:182px;
padding: 30px;
h4 {
  font-size: 18px;
  line-height: 60px;
  color: rgb(51,51,51);
  font-family: 'Raleway', sans-serif;
  font-weight: bold;
  margin-bottom: 0;
}
p {
  font-size: 13px;
  line-height: 20px;
  color: rgb(119,119,119);
  font-family: "Open Sans";
  margin-top: 0;
}
img {
  max-height: 50%;
  width: auto;
}
div {
  height: 182px;
  width: 164px;
  background-image: url(${props => props.src});
  background-repeat: no-repeat;
  background: contain;
  background-position: center;
  margin-right: 30px;
}
`

export default class Offer extends Component {
  render() {
    return (
      <Container src={this.props.data.icon.file.url}>
        <div>
        
        </div>
        <span>
          <h4>{this.props.data.title}</h4>
          <p>{this.props.data.description}</p>
        </span>
      </Container>
    )
  }
}
      